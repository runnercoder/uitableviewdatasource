//
//  MallTableViewDataSource.m
//  DCMall
//
//  Created by 吴恺 on 15/8/31.
//  Copyright (c) 2015年 wukai. All rights reserved.
//

#import "RCRTableViewDataSource.h"
@interface RCRTableViewDataSource ()
@property(nonatomic, copy) MallTableViewCellConfigureBlock block;
@property(copy, nonatomic, readwrite) NSArray *dataArray;
@property(copy, nonatomic, readwrite) NSString *cellIdentifier;
@end

@implementation RCRTableViewDataSource

- (instancetype)initDataSourceWithDataArray:(NSArray *)dataarray
                             cellIdentifier:(NSString *)identifier
                         cellConfigureBlock:
                             (MallTableViewCellConfigureBlock)block {
  self = [super init];
  if (self) {
    self.dataArray = dataarray;
    self.cellIdentifier = identifier;
    self.block = block;
  }
  return self;
}

- (instancetype)initDataSourceWithDataArray:(NSArray *)dataarray
                             cellIdentifier:(NSString *)identifier
                                  cellClass:(Class)cellClass
                         cellConfigureBlock:
                                (MallTableViewCellConfigureBlock)block {
    self = [super init];
    if (self) {
        self.dataArray = dataarray;
        self.cellIdentifier = identifier;
        self.cellClass = cellClass;
        self.block = block;
    }
    return self;
}

#pragma mark - public method
- (void)refreshDataSourceWithData:(NSArray *)data {
  self.dataArray = data;
}

#pragma mark - private method
- (id)dataForRowAtIndexPath:(NSIndexPath *)indexPath {
  return self.dataArray[indexPath.row];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
  return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell =
      [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
    if (!cell) {
        [tableView registerClass:self.cellClass forCellReuseIdentifier:self.cellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
    }
  id cellData = [self dataForRowAtIndexPath:indexPath];
  self.block(cell, cellData);
  return cell;
}

@end
