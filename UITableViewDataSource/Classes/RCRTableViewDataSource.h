//
//  MallTableViewDataSource.h
//  DCMall
//
//  Created by 吴恺 on 15/8/31.
//  Copyright (c) 2015年 wukai. All rights reserved.
//

#import <UIKit/UITableView.h>

typedef void (^MallTableViewCellConfigureBlock)(id cell, id cellData);

@interface RCRTableViewDataSource : NSObject<UITableViewDataSource>
@property(copy, nonatomic, readonly) MallTableViewCellConfigureBlock block;
@property(copy, nonatomic, readonly) NSArray *dataArray;
@property(copy, nonatomic, readonly) NSString *cellIdentifier;
@property (nonatomic, assign) Class cellClass;
- (instancetype)initDataSourceWithDataArray:(NSArray *)dataarray
                             cellIdentifier:(NSString *)identifier
                         cellConfigureBlock:
                             (MallTableViewCellConfigureBlock)block;
- (instancetype)initDataSourceWithDataArray:(NSArray *)dataarray
                             cellIdentifier:(NSString *)identifier
                                  cellClass:(Class)cellClass
                         cellConfigureBlock:
(MallTableViewCellConfigureBlock)block;
- (void)refreshDataSourceWithData:(NSArray *)data;
@end
