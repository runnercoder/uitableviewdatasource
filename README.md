# UITableViewDataSource

[![CI Status](http://img.shields.io/travis/starrykai/UITableViewDataSource.svg?style=flat)](https://travis-ci.org/starrykai/UITableViewDataSource)
[![Version](https://img.shields.io/cocoapods/v/UITableViewDataSource.svg?style=flat)](http://cocoapods.org/pods/UITableViewDataSource)
[![License](https://img.shields.io/cocoapods/l/UITableViewDataSource.svg?style=flat)](http://cocoapods.org/pods/UITableViewDataSource)
[![Platform](https://img.shields.io/cocoapods/p/UITableViewDataSource.svg?style=flat)](http://cocoapods.org/pods/UITableViewDataSource)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

UITableViewDataSource is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "UITableViewDataSource"
```

## Author

starrykai, starryskykai@gmail.com

## License

UITableViewDataSource is available under the MIT license. See the LICENSE file for more info.
